HEADERS = $(shell find . -name "*.h")
CFLAGS = -std=c99 -g -DDEBUG -Wall -Wextra -Wpedantic -Wfatal-errors

all: out/example

run: out/example
	./out/example

out/example: example.c $(HEADERS) | out/
	gcc $(CFLAGS) example.c -o out/example

doc: $(HEADERS) Doxyfile | html/
	doxygen Doxyfile

html/:
	mkdir -p html/

out/:
	mkdir -p out/

clean:
	rm -rf out/
	rm -rf html/
	rm -f foo
