#include <assert.h>
#include <stdio.h>

#include "c-utils.h"

int main() {
  puts ("c utils example...");

  SHOW_S (C_UTILS_VERSION);
  SHOW_F (5.0-10.0);
  SHOW_I (5-10);
  SHOW_U (5-10);
  SHOW_X (5-10);
  SHOW_S ("foo");

  SHOW_U (is_empty ("foo"));
  SHOW_U (is_empty (""));

  DEBUG_S ("foo");

  SHOW_LU (sizeof (i8));
  SHOW_LU (sizeof (i16));
  SHOW_LU (sizeof (i32));
  SHOW_LU (sizeof (i64));
  SHOW_LU (sizeof (isize));
  SHOW_LU (sizeof (u8));
  SHOW_LU (sizeof (u16));
  SHOW_LU (sizeof (u32));
  SHOW_LU (sizeof (u64));
  SHOW_LU (sizeof (usize));

  #define ONE_OBJECT 1
  u8 foo_write[] = { 'f', 'o', 'o', '\n' };
  write_file ("foo", foo_write, sizeof foo_write);
  u8* foo_read = read_file ("foo");
  char* foo_string = make_string (foo_read, sizeof foo_write);
  SHOW_S (foo_string);
  for (size_t i = 0; i < sizeof foo_write; i += 1) {
    assert (foo_write[i] == foo_read[i]);
  }

  const int t = 1;
  ASSERT(t);
  puts ("the following assertion should fail...");
  const int f = 0;
  ASSERT(f);

  puts ("...c utils example");
}
