with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "c-utils";
  src = lib.sourceByRegex ./. ["c-utils.h"];
  installPhase = ''
    mkdir -p $out/include
    cp ./c-utils.h $out/include
  '';
}
