/// \mainpage
/// > header-only utility macros, types, and functions

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

////////////////////////////////////////////////////////////////////////////////
//  macros
////////////////////////////////////////////////////////////////////////////////

#define C_UTILS_VERSION "0.0.1"

#define ASSERT(_x_) {                                       \
  if (!(_x_)) {                                             \
    printf ("%s:%i: %s: runtime ASSERT(" #_x_ ") failed\n", \
      __FILE__, __LINE__, __func__);                        \
    exit (EXIT_FAILURE);                                    \
  }                                                         \
}

#define ARRAY_LEN(_x_) (sizeof (_x_) / sizeof ((_x_)[0]))

#define SHOW(_x_, _format_) {              \
  printf (#_x_ ": " _format_ "\n", (_x_)); \
}
#define SHOW_S(_x_) {            \
  printf (#_x_ ": %s\n", (_x_)); \
}
#define SHOW_I(_x_) {            \
  printf (#_x_ ": %i\n", (_x_)); \
}
#define SHOW_LI(_x_) {            \
  printf (#_x_ ": %li\n", (_x_)); \
}
#define SHOW_U(_x_) {            \
  printf (#_x_ ": %u\n", (_x_)); \
}
#define SHOW_LU(_x_) {            \
  printf (#_x_ ": %lu\n", (_x_)); \
}
#define SHOW_X(_x_) {            \
  printf (#_x_ ": %x\n", (_x_)); \
}
#define SHOW_LX(_x_) {            \
  printf (#_x_ ": %lx\n", (_x_)); \
}
#define SHOW_F(_x_) {            \
  printf (#_x_ ": %f\n", (_x_)); \
}
#define SHOW_P(_x_) {            \
  printf (#_x_ ": %p\n", (_x_)); \
}

#ifdef NDEBUG
#define DEBUG_S(_x_)  {}
#define DEBUG_I(_x_)  {}
#define DEBUG_LI(_x_) {}
#define DEBUG_U(_x_)  {}
#define DEBUG_LU(_x_) {}
#define DEBUG_X(_x_)  {}
#define DEBUG_LX(_x_) {}
#define DEBUG_F(_x_)  {}
#define DEBUG_P(_x_)  {}
#else
#define DEBUG_S(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %s\n", (_x_)); \
}
#define DEBUG_I(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %i\n", (_x_)); \
}
#define DEBUG_LI(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %li\n", (_x_)); \
}
#define DEBUG_U(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %u\n", (_x_)); \
}
#define DEBUG_LU(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %lu\n", (_x_)); \
}
#define DEBUG_X(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %x\n", (_x_)); \
}
#define DEBUG_LX(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %lx\n", (_x_)); \
}
#define DEBUG_F(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %f\n", (_x_)); \
}
#define DEBUG_P(_x_) {                      \
  printf ("[DEBUG] " #_x_ ": %p\n", (_x_)); \
}
#endif

////////////////////////////////////////////////////////////////////////////////
//  typedefs
////////////////////////////////////////////////////////////////////////////////

typedef int8_t    i8;
typedef int16_t   i16;
typedef int32_t   i32;
typedef int64_t   i64;
typedef intptr_t  isize;
typedef uint8_t   u8;
typedef uint16_t  u16;
typedef uint32_t  u32;
typedef uint64_t  u64;
typedef uintptr_t usize;

////////////////////////////////////////////////////////////////////////////////
//  functions
////////////////////////////////////////////////////////////////////////////////

inline bool is_empty (const char* const string) {
  return strlen (string) == 0;
}

/// Read bytes contents from given filepath
u8* read_file (const char* const filepath) {
  #define SUCCESS 0
  #define ZERO_OFFSET 0
  #define ONE_OBJECT 1
  FILE* file = fopen (filepath, "rb");
  assert (file != NULL);
  assert (fseek (file, ZERO_OFFSET, SEEK_END) == SUCCESS);
  long size_bytes = ftell (file);
  rewind (file);
  u8* contents = malloc (size_bytes);
  assert (contents != NULL);
  assert (fread (contents, size_bytes, ONE_OBJECT, file) == ONE_OBJECT);
  assert (fclose (file) == SUCCESS);
  return contents;
}

/// Write byte contents to given filepath
void write_file (
  const char* const filepath,
  const u8* const   contents,
  const size_t      size_bytes
) {
  #define SUCCESS 0
  #define ONE_OBJECT 1
  FILE* file = fopen (filepath, "w");
  assert (file != NULL);
  assert (fwrite (contents, size_bytes, ONE_OBJECT, file) == ONE_OBJECT);
  assert (fclose (file) == SUCCESS);
}

/// Copy bytes (unchecked) into a newly-allocated null-terminated string (char*)
char* make_string (const u8* const contents, const size_t size_bytes) {
  char* string = malloc (size_bytes + 1);
  string[size_bytes] = 0;
  memcpy (string, contents, sizeof size_bytes);
  return string;
}
